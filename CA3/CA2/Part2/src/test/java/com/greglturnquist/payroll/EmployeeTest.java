package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void ensureCreationFailsWithInvalidValuesFirstName(String input) {

        assertThrows(IllegalArgumentException.class, () ->
                new Employee(input, "lastname", "description", "jobtitle", 5,"email@email.com"));

    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void ensureCreationFailsWithInvalidValuesLastName(String input) {

        assertThrows(IllegalArgumentException.class, () ->
                new Employee("input", input, "description", "jobtitle", 5, "email@email.com"));

    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void ensureCreationFailsWithInvalidValuesDesc(String input) {

        assertThrows(IllegalArgumentException.class, () ->
                new Employee("input", "input", input, "jobtitle", 5, "email@email.com"));

    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n"})
    public void ensureCreationFailsWithInvalidValuesTitle(String input) {

        assertThrows(IllegalArgumentException.class, () ->
                new Employee("input", "input", "input", input, 5, "email@email.com"));

    }


    @ParameterizedTest
    @ValueSource(ints = {-10, -2, -1})
    public void ensureCreationFailsWithInvalidValuesYears(int input) {

        assertThrows(IllegalArgumentException.class, () ->
                new Employee("input", "input", "input", "input", input, "email@email.com"));

    }
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n", "123", "email.pt", "dinis@sapo", "@", "@porto.pt", "ana sousa@fba.up.pt"})
    public void ensureCreationFailsWithInvalidValuesEmail(String input) {

        assertThrows(IllegalArgumentException.class, () ->
                new Employee("input", "input", "input", "input", 5, input));

    }


    @ParameterizedTest
    @ValueSource(ints = {10, 2, 0, 1})
    void testCreationSuccessDoesNotThrow(int input) {

        assertDoesNotThrow(() ->
                new Employee("input", "   input", "input  ", "input", input,"email@email.com"));

    }

    @ParameterizedTest
    @ValueSource(ints = {10, 2, 0, 1})
    void testCreationSuccessNotNull(int input) {
        Employee emp = new Employee("input", "   input", "input  ", "input", input, "email@email.com");
        assertNotNull(emp);

    }
}