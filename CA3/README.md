# Class Assignment 3 Report (CA3)

## Topic: Virtualization with Vagrant

## 1. Analysis, Design and Implementation

For this assignment, a previously created private Bitbucket repository was used. It can be accessed
at https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/.

Start date: 19th April End date: 9th May

This assignment was separated in two main parts:

* Part 1
* Part 2: Virtualization with Vagrant.

## 1.1. Part 1

### 1.1.1. Setting up the Virtual Machine
For this part a virtual machine was needed.
For that we used VirtualBox - a free hypervisor that runs in many host operating systems - to create a virtual
machine and install Linux (Ubuntu). The image used to install Linux is available at:
https://help.ubuntu.com/community/Installation/MinimalCD.

This Linux image is a minimal version that does not have a user interface (only command line is used).

The VM uses 2048 MB RAM and was the Network Adapter 2 was set up as Host_only Adapter.

![](Part1/images/vmsettings.png)

The Network adapter 2 was enabled. The network's IP is 192.168.56.1/24 as seen in the following image:

![](Part1/images/hostnetworkmanagerinfo.png)

So the selected IP for the VM should be in the range of this network.

After starting the VM, a few changes and updates were made, as following:

Update the packages repositories:
```
sudo apt update
```
Install the network tools:
```
sudo apt install net-tools
```
Edit the network configuration file to set up the IP:
```
sudo nano /etc/netplan/01-netcfg.yaml
```
The contents of the file were changed to add the IP of the VM, in the range of the host network,
in this case 192.168.56.5/24

![](Part1/images/yamledit.png)

Apply the new changes:
```
sudo netplan apply
```
Install openssh-server so that we can use ssh to open secure terminal sessions to the
VM (from other hosts):
```
sudo apt install openssh-server
```
Enable password authentication for ssh:
```
sudo nano /etc/ssh/sshd_config
```
Uncomment the line PasswordAuthentication yes.
```
sudo service ssh restart
```
Install an ftp server so that we can use the FTP protocol to transfers files to/from
the VM (from other hosts):
```
sudo apt install vsftpd
```
Enable write access for vsftpd:
```
sudo nano /etc/vsftpd.conf
```
Uncomment the line write_enable=YES
```
sudo service vsftpd restart
```

We can now connect to the VM using SSH, from our host machine.
In the host terminal, type:

```
ssh miguel@192.168.56.5
```
(Replace with proper name and IP address).

We can also use FTP server to use an FTP application such as FileZilla, to transfer files.

Before we start the actual exercise we also need up install git and java jdk:

```
sudo apt install git
sudo apt install openjdk-11-jdk-headless
```

### 1.1.2. Using the VM to run previous applications 

With a terminal open in the host machine, as explained before, connected to the VM using ssh, we started
by cloning the individual repository inside the VM, using the command:
```
git clone https://miguelar97@bitbucket.org/miguelar97/devops-21-22-atb-1151394.git
```
### CA1

Then we changed directory to the CA1 basic folder:

![](Part1/images/ca1basicpath.png)

If we try to run the application now, with the command:
```
./mvnw spring-boot:run
```
We get an error message in the command line stating that we don't have permissions.
To solve that we must change permissions using the command:
```
chmod a+x ./mvnw
```

Now we can execute the application again and we see that it runs, as shown below:

![](Part1/images/ca1part1mvnwrunresult.png)

###CA2 Part1

Before we proceed we need to copy one file that is missing from the repository, using FileZilla.
We can connect with the VM by using the IP, username and password as follows:

![](Part1/images/filezillaFTPconfig.png)

Now, in the path devops-21-22-atb-1151394\CA2\Part 1\gradle\wrapper\ we copy the file
gradle-wrapper.jar to the similar folder inside the VM.

Changing the directory to the CA2 Part 1 folder, now we want to run the gradle application.

First we need to change permissions of the gradlew file, like explained for the CA1 exercise:

```
chmod a+x ./gradlew
```

For that we first use the command:
```
./gradlew build
```

This will execute all tasks inside gradle.build:

![](Part1/images/gradlewbuild.png)

For some reason the copySources and zipSources tasks are not running correctly, maybe
because we are using ubuntu server and this does not have a Desktop.

Now we can run the server part of the application, using the runServer task that was created in the previous
assignments.

```
./gradlew runServer
```

![](Part1/images/runservertask.png)

This will start the server side of the app and allows the Host Machine to create
the client side application.

Before that, we need to change the build.gradle file in the host machine to be able to
connect to the VM:

![](Part1/images/build.gradleRunClientChange.png)

We changed the runClient task to add the IP of the VM.

Now, in the command line of the host machine, without connecting to the VM, we
changed directory to the CA2 Part 1 folder and executed the runClient task:

```
./gradlew runClient
```

![](Part1/images/userOneChatApp.png)

After this, in another terminal window we repeated the process for user two:

![](Part1/images/userTwoChatApp.png)

Now both users can communicate, using the VM as a server:

![](Part1/images/usersCommunicating.png)

For web projects you should access the web applications from the browser in your
host machine, since the server usually does not have a user interface and cannot display the contents.

For projects such as this simple chat application you should execute the server
inside the VM and the clients in your host machine, because the client side uses a pop up window that needs to display
it in the Desktop, feature that the server does not usually have, so we could not use the chat app properly.

###CA2 Part2

Changing the directory to the CA2 Part 2 folder, now we want to run the gradle version of the basic spring tutorial.

First we need to change permissions of the gradlew file, like explained before.

```
chmod a+x ./gradlew
```

Now we use the gradle build:
```
./gradlew build
```

![](Part1/images/gradlewbuildspring.png)

And then we use gradle bootRun task to execute the application:

![](Part1/images/gradlewbootrun.png)
We can see the result in the host browser:

![](Part1/images/gradlewbootrunresult.png)

## 1.2. Part 2 

## 1.2.1 Running provided project with vagrant
###Introduction
For this part of the CA3 assignment, the goal is to use Vagrant to run an application linked to an H2 database that
runs in a virtual machine, with a second virtual machine responsible for showing the front end. Vagrant
should execute the tutorial spring boot application, gradle "basic" version developed in CA2 part2.

Vagrant is an open-source tool that allows the creation, configuration, and management of virtual machines 
through an easy to use command interface. Essentially, it is a layer of software installed between a virtualization 
tool (such as VirtualBox, Docker, Hyper-V) and a Virtual Machine.

It is often used in software development to ensure all team members are building for the same configuration. 
Not only does it share environments, but it also shares code as well. This allows the code from one developer
to work on the system of another, making collaborative and cooperative development possible.

###VagrantFile

The VagrantFile provided for this assignment in the class materials uses the spring application available 
at https://bitbucket.org/atb/tut-basic-gradle.

The VagrantFile is prepared to setup two VMs:

web VM:
Executes the web application inside Tomcat8

db VM:
Executes the H2 database as a server process. The web application connects to this VM.

### Steps to execute the application with Vagrant

The project with the provided VagrantFile can be cloned in https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/.

After installing both Vagrant and VirtualBox and creating a folder in the computer with the provided VagrantFile,
we need to use the following command line in the proper path, to create two VMs (db and web).

```
vagrant up
```

After this, in the host you can open the spring web application
using one of the following options:

http://localhost:8080/basic-0.0.1-SNAPSHOT/

http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

![](Part 2 Vagrantfile/images/webwithprovidedproject.png)
You can also open the H2 console using one of the following urls:

http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/

http://192.168.56.10:8080/basic-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.56.11:9092/./jpadb

![](Part 2 Vagrantfile/images/dbwithprovidedproject.png)

## 1.2.2 Running CA2 part2 application with vagrant

To run the CA2 part2 exercise application with vagrant, we first need to copy the provided Vagrantfile to the 
repository (inside the folder for this assignment). 

![](Part 2 Vagrantfile/images/ca3p2filestructure.png)

Now we need to change the Vagrantfile so that it uses the CA2 Part2 version of the
spring application and not the one used before. 
For that, we change the line where Vagrant clones the repository to copy our own.

* Before changes:

![](Part 2 Vagrantfile/images/gitclonebeforechanges.png)

* After changes:

![](Part 2 Vagrantfile/images/gitcloneafterchanges.png)

So we don't actually change the previous assignment code, we will simply make a copy of CA2 into the CA3 folder.
After this a commit was made to the repository with these changes.

NOTE:
There is an issue when cloning a private repository in the provision section of the Vagrantfile.
When we execute a git clone of a private repository, git will ask for authentication
using a prompt for reading the password.
This prompt for reading the password will break the provision script, since it should
execute without any user interaction.
The simplest way to avoid this issue is to make the repository public (in Bitbucket go to 
Repository Settings/Repository Details).

Now, before trying to run the VMs with Vagrant we first need to do the changes
necessary so that the spring application uses the H2 server in the db VM.

For that, we used the content from the application.properties file inside the provided tut-basic-gradle project,
as follows:

![](Part 2 Vagrantfile/images/applicationproperties.png)

After this we committed the changes.

Now, we are going to try to run Vagrant, but before that we need to clean the previously created VMs, with the command.

```
vagrant -f destroy
```

After this we can change directory to CA3\CA2\Part2 and try to use the vagrant up command.

This resulted in various errors: build failed, frontend errors, nodejs errors etc.
Because of this a few changes were made to the Vagrantfile and build.gradle file.

![img.png](Part%202 Vagrantfile/images/newprovision.png)

Because the provided VagrantFile used a box compatible with only java 8 jdk and our software runs java 11, we changed to another box, as follows:

![](Part 2 Vagrantfile/images/bentobox.png)

We also made a few changes in the build.gradle file:

* plugins
![](Part 2 Vagrantfile/images/pluginsbuildgradle.png)

  
* frontend

![](Part 2 Vagrantfile/images/frontend.png)

After this we committed changes and try to run the application again with Vagrant.

The application ran as intended:

![](Part 2 Vagrantfile/images/vagrantupworked.png)

And the frontend was generated.

## 2. Analysis of an Alternative - Vagrant with VMware

VirtualBox is the default provider for Vagrant. However, Vagrant supports other hypervisors.
We can change Vagrant's default provider by setting the VAGRANT_DEFAULT_PROVIDER environmental variable.
To implement the alternative solution, we will be using VMWare.

##2.1. Vagrant with VirtualBox vs Vagrant with VMware
Both hypervisors are very similar in features, and both serve the purpose well. 
However, there are some differences between offered functionalities, that may be important to take into account 
when working on bigger projects.
 
Based on this website https://www.jeffgeerling.com/blogs/jeff-geerling/vagrant-vmware-7-vs-virtualbox-5-benchmarks, we 
can conclude that:

VMware is better when it comes to network throughput, around a 40% improvement, although most developers won't benefit 
from this because they don't rely on a lot of bandwidth. On a lesser scale, this difference would not be noticeable, 
but it would have an influence on enterprise project performance.
Both solutions have similar performance when it comes to CPU and RAM workloads.

Based on this other website https://www.interviewbit.com/blog/vmware-vs-virtualbox/#:~:text=The%20most%20obvious%20distinction%20between,for%20example%2C%20are%20not%20supported,
we can conclude that:
Regarding the user interface, VirtualBox is much more user-friendly while VMware has a 
complicated user interface.

When is comes to pricing, VirtualBox is free and opensource, while VMware is a paid service.

Regarding virtualization, VirtualBox provides virtualization for both hardware and software, while VMware
only provides hardware virtualization. 

So, For enterprises that already have a VMware setup, can afford the licence and support costs, 
and desire consistent performance, VMware remains the preferred solution.

## 3. Implementation of the Alternative

## 3.1. Part 1

This part of the assignment did not require an alternative.

## 3.2. Part 2

* Installation of the Vagrant VMware provider:

Install VMware Workstation Pro (30 days free trial):
https://www.vmware.com/products/workstation-pro.html

Vagrant VMware Utility must also be installed:
https://www.vagrantup.com/vmware/downloads

Finally, it is necessary to install the Vagrant VMware provider plugin, running this shell command:

```
vagrant plugin install vagrant-vmware-desktop
```

Then update to make sure we are using the latest version:
```
vagrant plugin update
```

* Configuring the VM with VagrantFile and build.gradle:

The box we used for the part 2 exercise, bento/ubuntu-18.04, is supported by VMware,
so we may keep using it.

We can specify the provider to use in the Vagranfile, in the config of each VM (db and web), as
seen in the images below:

![](Part 2 Vagrantfile/images/newconfigfordbvm.png)

![](Part 2 Vagrantfile/images/newconfigforwebvm.png)

We cannot use the same network and the same IP addresses for the new machines as we used 
for the previous CA3 Part2 assignment, or the IPs will collide with VirtualBox Network Adapters.

So, we need to change the IP addresses of the new machines in order to have a different network. This
can also be seen in the images above: we changed the line
```
db.vm.network "private_network", ip : "192.168.58.11" 
```
and
```
web.vm.network "private_network", ip : "192.168.58.10"
``` 
We also have to change the IP address of db machine specified at application.properties to correspond 
to the one we defined in the Vagrantfile:

![](Part 2 Vagrantfile/images/aplicationspropertieschangedb.png)

We also need to change the host ports of our machines, so we can run them simultaneously with VirtualBox if needed.

So in this next image we changed db vm ports to 8082 and 8083:

![](Part 2 Vagrantfile/images/newportsdbvm.png)

And in the web vm to 8081:

![](Part 2 Vagrantfile/images/newportsforwebvm.png)

If we run our application at this point, we get a 404 NOT FOUND error code from the server, because Spring will 
by default start at port 8080.

To solve this problem, we must add a new configuration to the top of application.properties, 
specifying the port of the host machine that redirects to the port of the guest machine where 
the application is running:

![](Part 2 Vagrantfile/images/newpropertieserverport.png)

With these changes we can now change directory to the alternative folder's Vagrantfile and use the command:

```
vagrant up --provision
```

to start the two machines with VMware.

After this, in the host you can open the spring web application
using one of the following options:

http://localhost:8081/basic-0.0.1-SNAPSHOT/

http://192.168.58.10:8081/basic-0.0.1-SNAPSHOT/

You can also open the H2 console using one of the following urls:

http://192.168.58.11:8080/basic-0.0.1-SNAPSHOT/

http://192.168.58.11:8080/basic-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.58.11:9093/./jpadb

This concludes the assignment.