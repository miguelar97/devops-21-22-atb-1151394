# Individual Repository for DevOps

This repository contains the files for all the class assignemts of DevOps.

* [Class Assignment 1](https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/src/master/CA1/README.md)

* [Class Assignment 2](https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/src/master/CA2/README.md)

* [Class Assignment 3](https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/src/master/CA3/README.md)

* [Class Assignment 4](https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/src/master/CA4/README.md)

* [Class Assignment 5](https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/src/master/CA5/README.md)

* [Class Assignment 6](https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/src/master/CA6/README.md)
