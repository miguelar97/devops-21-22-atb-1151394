# Class Assignment 2 Report (CA2)

## Topic: Build Tools with Gradle

## 1. Analysis, Design and Implementation

For this assignment, a previously created private Bitbucket repository was used. It can be accessed
at https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/.

Start date: 28th March End date: 11th April

This assignment was separated in two main parts:

* Part 1: practice gradle using a simple example.
* Part 2: build tools with Gradle.

## 1.1. Part 1

An example gradle application was used for this assignment, available
at https://bitbucket.org/luisnogueira/gradle_basic_demo/.

For this part of the assignment, several tasks were involved:

1) Add a new task to execute the server.
2) Add a simple unit test and update the gradle script so that it is able to execute the test.
3) Add a new task of type Copy to be used to make a backup of the sources of the application. It should copy the
   contents of the src folder to a new backup folder.
4) Add a new task of type Zip to be used to make an archive (i.e., zip file) of the sources of the application. It
   should copy the contents of the src folder to a new zip file.
5) At the end of the part 1 of this assignment the repository should be marked with the tag ca2-part1.

After copying the example application to the Part 1 folder inside the CA2 folder, a first commit was pushed with the new
contents.

A new task was created inside the build.gradle file, to execute the server.

```
task runServer(type: JavaExec, dependsOn: classes) {

    group = "DevOps"
    description = "Launches the chat server at port 59001 "

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatServerApp'

    args '59001'
}
```

This task, called runServer, will serve the same function as the command present in the README file of the original
application:

```
java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

The runServer task uses the ChatServerApp to execute the server. As this class takes as parameter String args[0], only
one input String is accepted: for that, unlike the task runClient, only the port 590001 is given to the task and not
localhost and 59001.

To test this task a simple unit test was added to the app:

```
@Test
public void testAppHasAGreeting() {
App classUnderTest = new App();
assertNotNull("app should have a greeting", classUnderTest.getGreeting());
}
```

This test will run when the build is executed. The test was also added to the build.gradle file:

```
test (){
    useJUnit()
    minHeapSize = "128m"
    maxHeapSize = "512m"
}
```

Then, two new tasks were made:

a) A copySources task, of the Copy type, that copies all files under the src folder:

```
task copySources(type: Copy) {
    from 'src'
    into 'backup'
}
```

b) A zipSources task, of the Zip type, that creates a zip file with all files in the src folder:

```
task zipSources(type: Zip) {
    from 'src'
    archiveFileName = 'backup.zip'
    destinationDir(file('backupZip'))
}
```

After this, the app was run again to make sure it functions properly and the two last tasks were run to create a backup
of the sources of the application.

This concluded the first part of this exercise. The changes were committed and the repository was tagged as ca2-part1.

## 1.2. Part 2

For this part, the main goal is to convert the "basic" version of the Tutorial application (used in CA1) to Gradle,
instead of Maven.

First, a new branch was created named tut-basic-gradle.

```
git branch tut-basic-gradle
```

Then we moved to the new branch:

```
git checkout tut-basic-gradle
```

To create a new gradle spring boot project, we used https://start.spring.io/ with the following configurations:

![](Part 2/images/start-spring-gradle-project-dependencies.png)

The generated project/application was put inside CA2/Part2 subfolder of this repository to be used for the exercise.

The available tasks can be checked by executing the command:

```
./gradlew tasks
```

After this the generated src folder was deleted, since we will be using the code included in the tutorial basic project.
The basic tutorial src folder was then copied to the spring project. The webpack.config.js and package.json files were
also copied to the spring project.

The src/main/resources/static/built folder was deleted manually because this folder will be later created by the webpack
tool.

The application can be run with the command

```
./gradlew bootRun
```

The http://localhost:8080/ page will still be empty because the gradle project is missing the plugin to work with the
frontend code.

The following plugin was added so gradle can manage the frontend. This was done by editing the build.gradle file in the
plugins section.

```
id "org.siouan.frontend-jdk11" version "6.0.0"
```

The following code was also added to configure the previous plugin, in the build.gradle file.

```
frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}
```

Finally, the scripts section in the package.json file was edited to configure the execution of webpack:

```
"scripts": {
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
},
```

Use the following command to build the gradle project:

```
./gradlew build.
```

This also generated the frontend code by executing the tasks related to the frontend.

The application can now be executed with the command:

```
./gradlew bootRun
```

To finish this assignment, two gradle tasks were developed:

a) A task to copy the generated jar file to a folder named "dist" located at the project root folder level;

b) A task to delete all files generated by webpack, that should be run automatically by gradle before the task clean.
The files are located at src/resources/main/static/built.

The first task was developed as:

```
task copyJar(type: Copy) {
	from '/gradle/wrapper/gradle-wrapper.jar'
	into 'dist'
}
```

The task is of type Copy, and copies the file gradle-wrapper.jar inside the wrapper folder, which exists inside the
gradle folder.

The copied file is put into the dist folder. This folder will be created upon the first time the task is run.

The second task was developed as:

```
task deleteBuiltFolder(type: Delete){
	delete '/src/main/resources/static/built'
}
```

This task is of type Delete, and will delete the build folder that is located inside the static folder.

To make this task run before the clean task, the following line was added:

```
clean.dependsOn deleteBuiltFolder
```

Clean is a default task contained in gradle. With this line, the clean task won't run without first executing the
deleteBuiltFolder task.

This concluded the second part of the exercise. The features were tested to make sure everything is working as intended
before commiting, pushing and merging with master.

## 2. Analysis of an Alternative

A good alternativa to gradle is maven. Maven is an open-source build automation tool used primarily for Java projects.
It is a powerful project management tool, based on POM - project object model. It is used for building projects,
managing dependencies and documentation.

It uses an XML file to describe the project that you are building, the dependencies of the software in regard to
third-party modules and parts, the build order, as well as the needed plugins. There are pre-defined targets for tasks
such as packaging and compiling.

## 2.1 Maven vs Gradle

Gradle uses domain-specific language based on the programming language Groovy, differentiating it from Maven, which uses
XML for its project configuration. It also determines the order of tasks run by using a directed acyclic graph.

Other than this, there are some fundamental differences between the two tools:

* While gradle performs using mainly tasks, based on an acyclic graph of task dependencies, Maven uses a fixed and
  linear model of phases. Maven associates goals with project phases. Multiple goals take on the form of an ordered
  list. Gradle allows task exclusions, transitive exclusions, and task dependency inference. Gradle also has advanced
  features for task ordering and finalizers, among others, which Maven does not.
* While both allow for multi-module builds to run in parallel, Gradle allows for incremental builds because it checks
  which tasks are updated or not. This allows for much shorted build time when a task is not changed from the last
  build.

Other performance features on Gradle that do not exist on Maven:

*Incremental compilations for Java classes: Gradle analyses sources and classes, recompiling only if necessary
*Compile avoidance for Java: avoids repeated class compilation or test runs
*The use of APIs for incremental subtasks
*A compiler daemon that also makes compiling a lot faster: continuously runs Gradle in the background ready to execute a
build

Gradle ensures that two or more projects using the same cache will not overwrite each other, by keeping repository
metadata along with cached dependencies.

In summary, while Maven can be an alternative to Gradle, Gradle is faster, more concise, easier to customise, promotes
build code reuse and improves the developers' productivity.

## 3. Implementation of the Alternative

## 3.1. Part 1

This part of the assignment did not require an alternative.

## 3.2. Part 2

To use Maven goals instead of the Gradle tasks, the pom.xml file must be edited.

To develop an alternative to the copyJar task, the following maven plugin was used:
```
<plugin>
  <groupId>org.codehaus.mojo</groupId>
    <artifactId>build-helper-maven-plugin</artifactId>
      <version>1.3</version>
        <executions>
          <execution>
            <id>copy-jar</id>
              <phase>package</phase>
                <goals>
                  <goal>attach-artifact</goal>
                </goals>
                <configuration>
                  <artifacts>
                    <artifact>
                      <file>${project.build.directory}/maven.wrapper.jar</file>
                        <type>jar</type>
                    </artifact>
                  </artifacts>
                </configuration>
          </execution>
        </executions>
</plugin>

To develop an alternative to the deleteBuiltFolder task, the following maven plugin was used:
```

```
<plugin>
  <artifactId>maven-clean-plugin</artifactId>
    <version>3.1.0</version>
      <executions>
        <execution>
          <id>delete-webpack</id>
            <phase>pre-clean</phase>
              <goals>
                <goal>clean</goal>
              </goals>
              <configuration>
                <filesets>
                  <fileset>
                    <directory>src/resources/static/built</directory>
                      <includes>
                        <include>*/</include>
                      </includes>
                   </fileset>
                  </filesets>
              </configuration>
        </execution>
      </executions>
</plugin>
```

This plugin is executed during the pre-clean phase and changes the clean goal to delete 
the built folder under the src/resources/static/built directory. By using it in the pre-clean phase,
maven executes the goal prior to the actual project cleaning.

This concludes the alternative to this part of the assignment.

















