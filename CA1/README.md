# Class Assignment 1 Report (CA1)
## Topic: Version Control with Git
## 1. Analysis, Design and Implementation



For this assignment, a previously created private Bitbucket repository was used.
It can be accessed at https://bitbucket.org/miguelar97/devops-21-22-atb-1151394/.

Start date: 14th March
End date: 28th March

The "basic" folder of the Tutorial React.js and Spring Data REST application was used for this assignment.

This assignment was separated in two main parts:

* Part 1: no branches.
* Part 2: with branches.

##1.1. Part 1 - no branches

This part of the assignment was and should be developed without using branches. Everything was done in the master branch.

To start this part of the assignment a copy of the Tutorial React.js and Spring Data REST Application was made into a new folder named CA1.

After this, we need to commit the changes, with the command:

```
git commit -a -m "Add a new folder called CA1 with the contents of the basic folder from the Tutorial React.js and Spring Data REST project."
```

And then push the changes to the remote:

```
git push origin master
```

Tags were used to mark the versions of the application, following the pattern major.minor.revision (e.g. v1.0.0).

This initial version was tagged as v1.1.0.

```
git tag v1.1.0
```

And then the tag was pushed to the remote:  

```
git push origin --tags
```

So our last commit will be tagged in the remote as v1.1.0.

After this, there was the need to develop a new feature to add a new field to the application.
A new field was added to record the years of the employee in the company, jobYears.

This required:

* Add support for the new field.
* Add unit tests for testing the creation of Employees and the validation
  of their attributes (for instance, no null/empty values). For the new field, only integer
  values should be allowed.
* Debug the server and client parts of the solution.

Changes were made in the application to fit these requirements. The DatabaseLoader class was changed to add this new 
field, too, as well as the app.js file. 

Several commits were made.

Then the following command was run in the command line to run the application and see the results in the browser, using http://localhost:8080/.

```
mvnw spring-boot:run
```

In the images below we can see the implemented change in the application, with one and more Employees created with
DatabaseLoader.

![](basic/images/ca1-part1-Proof1.png)

![](basic/images/ca1-part1-Proof2.png)

Incorrect data was also loaded to DatabaseLoader (negative job years), in order to trigger the validations of the new field
to test if the application is running correctly, as shown in the image below.

![](basic/images/ca1-part1-DatabaseLoaderInvalidJobYears.png)

Then the application was run again, with the command:

```
mvnw spring-boot:run
```
As incorrect data was loaded, an exception was expected to be thrown, which was confirmed in the
command line output:

![](basic/images/ca1-part1-debugging.png)

This confirmed the application is running as intended.

After debugging, the final changes were committed and pushed to the remote repository, as done previously.

This final version was tagged as v1.2.0, as well as ca1-part1, and pushed to remote repository.

```
git tag v1.2.0
git tag ca1-part1
```
```
git push origin --tags
```

This step concludes the core exercise.

##1.1. Part 2 - with branches

This part of the assignment was and should be developed using branches.

A branch is used to develop new features for the application. In this case, there was a need to add a new email field
to the application.

Branches should be named after the feature, so the branch was created with the name "email-field", with the command:

```
git branch email-field
```

After the branch creation we need to change to that branch so we are working in the branch instead of master.

For that the following command was used:

```
git checkout email-field
```

After this all commits are pushed to the branch instead of master, as long as the branch is not changed in the commnad
line.

A new field was added for the employee's e-mail.

This required:

* Add support for the new field.
* Add unit tests for testing the creation of Employees and the validation
  of the new attribute (no null/empty values). 
* Debug the server and client parts of the solution.

Changes were made in the application to fit these requirements. The DatabaseLoader class was changed to add this new
field, too, as well as the app.js file.

Then the following command was run in the command line to run the application and see the results in the browser, using http://localhost:8080/.

```
mvnw spring-boot:run
```

In the images below we can see the implemented change in the application:

![](basic/images/ca1-part2-proof1.png)

Incorrect data was also loaded to DatabaseLoader (empty email field), in order to trigger the validations of the new field
to test if the application is running correctly, as shown in the image below.

![](basic/images/ca1-part2-DatabaseLoaderInvalidEmailField.png)

Then the application was run again, with the command:

```
mvnw spring-boot:run
```
As incorrect data was loaded, an exception was expected to be thrown, which was confirmed in the
command line output:

![](basic/images/ca1-part2-debugging.png)

This confirmed the application is running as intended.

After debugging, the final changes were committed and pushed to the remote repository, as done previously, to the
created email-field branch:

```
git commit -a -m "Add new Email field to employee. Add unit tests."
```

```
git push origin email-field
```

This commit was tagged as v1.3.0.

```
git tag v1.3.0
```
```
git push origin --tags
```
Then the email-field branch was merged into the master branch.
```
git merge email-field
```
Now the remote only has one branch once again.

After this, there was the need to create a new branch to fix the new email field, as the created validations
only covered cases when the field was empty or null. A new branch was created, fix-invalid-email, to add new validations
for the email format (e.g., an email must have the @ sign).

This was considered a bug fix. Branches should be created in case of bug fixes, too.

```
git branch fix-invalid-email
```
```
git checkout fix-invalid-email
```
To add the new validations and fix the bug, it was required to:

* Add a new method using a regular expression that validates the correct email format.
* Add new tests to validate that only a correctly formatted email String is accepted.
* Debug the server and client parts of the solution.

After these steps, the changes were committed and pushed to the fix-invalid-email branch:
```
git push origin fix-invalid-email
```
This commit was tagged as v1.3.1 and the tag was pushed to the remote.
```
git tag v1.3.1
```
```
git push origin --tags
```
Then the fix-invalid-email branch was merged into the master branch.
```
git merge fix-invalid-email
```

This concluded the exercise, so the repository was tagged with the tag ca1-part2:
```
git tag ca1-part2
```

## 2. Analysis of an Alternative

An alternative to Git is Mercurial (Hg). Both Mercurial and Git are free DVCS - distributed version control systems.

Both allow developers to bring the repository code to the personal machines,
perform their work items, and then put it back into a central server.


## 2.1 Main differences

### 2.1.1 Syntax

While Git and Mercurial work similarly, Git is more complex than Mercurial. Mercurial’s syntax is simpler, so
it can be easier to adopt.

In the image below there is a simple comparison of the two software commands.

![](basic/images/MercurialAndGitCommands.png)

### 2.1.2 Security

By default, Mercurial does not allow the user to change the history. Mercurial allows the user to rollback to last pull
or commit and change it, but not further back.

In comparison, Git allows all involved developers to change the version history.

This means Git is more flexible, but can become hard to secure the codebase with new/inexperienced developers. This
leads to the conclusion that Mercurial is safer for new developers, while Git can be better for experienced developers.

### 2.1.3 Branching

Git’s branching model is more effective. In Git, branches are only references to a certain commit. Git allows the user
to create, delete, and change a branch anytime, without affecting the commits. 

In Mercurial, branches refer to a linear line of consecutive changesets.
Changesets refer to a complete set of changes made to a file in a repository.

Mercurial embeds the branches in the commits, where they are stored.
This means that branches cannot be removed because that would alter the history.

It requires the user to take added care not to push code to the wrong branch.


### 2.1.4 Staging area

Git supports the idea of a staging area, which is also known as the index file.

Staging is the practice of adding files for the next commit. 
It allows the user to choose which files to commit next.
This is useful when you don't want to commit all changed files together.

In Mercurial, there is no index or staging area before the commit. Changes are committed as they are in the
working directory. If a similar option is required in Mercurial, the user needs to use extensions.

## 3. Implementation of the Alternative

##Part 1 - no branches

To implement the alternative, first the Mercurial software (6.1) was installed in the local machine.

There was also the need to choose a remote repository that works well with Mercurial. For
that, HelixTeamHub was used. The remote link is:

    https://1151394isepipppt@helixteamhub.cloud/petite-bank-679/projects/devops_ca1/repositories/mercurial/devops_ca1

After that, a new copy of the basic tutorial folder was added to a new folder called CA1 to start the project.

In the command line in the correct path to that folder, the following command was used to add all untracked files 
to the Mercurial remote:
```
hg add
```
Then, a commit was made with those changes:
```
hg commit -m "Initial commit"
```
And the changes were pushed to the HelixTeamHub remote:
```
hg push https://1151394isepipppt@helixteamhub.cloud/petite-bank-679/projects/devops_ca1/repositories/mercurial/devops_ca1
```
This initial version was tagged with v1.1.0:
```
hg tag v1.1.0
```
After this the necessary changes were made to the application to meet the requirements for the first part of the exercise:

* Add support for the new field jobYears.
* Add unit tests for testing the creation of Employees and the validation
  of their attributes (for instance, no null/empty values). For the new field, only integer
  values should be allowed.
* Debug the server and client parts of the solution.

After the changes and debugging a new commit was made and pushed to the remote, before being tagged as v1.2.0 and
ca1-part1.
```
hg commit -m "Add support for new field jobYears. Add unit tests for the Employee creation and for all field validations."
```
```
hg tag v1.2.0
hg tag ca1-part1
```
```
hg push https://1151394isepipppt@helixteamhub.cloud/petite-bank-679/projects/devops_ca1/repositories/mercurial/devops_ca1
```
This concluded the first part of the exercise.

##Part 2 - with branches

This part of the exercise was not made with the alternative.






















