package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class EmployeeTest {

    //SUT - System under test

    @Test
    void validateEmployeeFirstName_NullValue(){
        //SUT: Employee constructor and validateEmployeeFirstName() method
        //Arrange
        String firstName = null;
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
            Assertions.assertThrows(NullPointerException.class, () -> {
                Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
                ;
            });
    }

    @Test
    void validateEmployeeFirstName_EmptyString(){
        //SUT: Employee constructor and validateEmployeeFirstName() method
        //Arrange
        String firstName = "";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeFirstName_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeFirstName() method
        //Arrange
        String firstName = "    ";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeLastName_NullValue(){
        //SUT: Employee constructor and validateEmployeeFirstName() method
        //Arrange
        String firstName = "First";
        String lastName = null;
        String description = "Description";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeLastName_EmptyString(){
        //SUT: Employee constructor and validateEmployeeLastName() method
        //Arrange
        String firstName = "First";
        String lastName = "";
        String description = "Description";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeLastName_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeLastName() method
        //Arrange
        String firstName = "First";
        String lastName = "     ";
        String description = "Description";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeDescription_NullValue(){
        //SUT: Employee constructor and validateEmployeeDescription() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = null;
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeDescription_EmptyString(){
        //SUT: Employee constructor and validateEmployeeDescription() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeDescription_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeDescription() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "  ";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeJobTitle_NullValue(){
        //SUT: Employee constructor and validateEmployeeJobTitle() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = null;
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });

    }

    @Test
    void validateEmployeeJobTitle_EmptyString(){
        //SUT: Employee constructor and validateEmployeeJobTitle() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeJobTitle_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeJobTitle() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "     ";
        String email = "email@isep.ipp.pt";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeJobYears_NegativeInteger(){
        //SUT: Employee constructor and validateEmployeeJobYears() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = -1;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeEmail_NullValue(){
        //SUT: Employee constructor and validateEmployeeEmail() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "JobTitle";
        String email = null;
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });

    }

    @Test
    void validateEmployeeEmail_EmptyString(){
        //SUT: Employee constructor and validateEmployeeEmail() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "JobTitle";
        String email = "";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeEmail_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeEmail() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "JobTitle";
        String email = "     ";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validateEmployeeEmail_RequireAtSign(){
        //SUT: Employee constructor and validateEmployeeEmail() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "JobTitle";
        String email = "emailemail.com";
        int jobYears = 10;

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
            ;
        });
    }

    @Test
    void validEmployeeState_CompareTwoEmployees(){
        //SUT: Employee constructor and equals/hashCode methods
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";
        String email = "email@isep.ipp.pt";
        int jobYears = 0;

        //Act
        Employee employeeTest = new Employee(firstName, lastName, description, jobTitle, jobYears, email);
        Employee employeeTest2 = new Employee(firstName, lastName, description, jobTitle, jobYears, email);

        //Assert
        assertEquals(employeeTest,employeeTest2);
    }
}