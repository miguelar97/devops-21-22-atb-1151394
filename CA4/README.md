# Class Assignment 4 Report (CA4)

## Topic: Containers with Docker

## 1. Analysis, Design and Implementation

## 1.1. Part 1

The goal of this class assignment is to build containers to run the chat server, so that we can connect chat 
clients to it from our host machine. We developed two versions:

In a first approach we built the chat server "inside" the Dockerfile, that is we cloned the chat app in the Dockerfile
and built the gradle app.

The created image is myimagev1. The Dockerfile is inside the dockerfilev1 folder which is inside the CA4 folder.

On a second approach we build the chat server in our host machine and copied the created jar into the Dockerfile.

The created image is myimagev2. The Dockerfile is inside the dockerfilev2 folder which is inside the CA4 folder.


After creating the images we were supposed to upload them to Docker Hub using the commands:

```
docker tag local-image:tagname new-repo:tagname
docker push new-repo:tagname
```

However, an error kept occurring stating we didn't have access to upload. So we kept the images only locally.

![](images/deniederrorwhenpushingtodockerhub.png)

### First image

For the first image, the Dockerfile needed to:

* Install Java JDK11
* Install git
* Clone the chat demo app
* Grant permissions to the gradlew file
* Build the application
* Finally, execute the application exposing the 59001 port

The Dockerfile ended up like:

```
FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y openjdk-11-jdk-headless
RUN apt-get install -y git

RUN git clone https://miguelar97@bitbucket.org/luisnogueira/gradle_basic_demo.git

WORKDIR gradle_basic_demo

RUN chmod u+x gradlew
RUN ./gradlew clean build

EXPOSE 59001

RUN java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

We started with a basic ubuntu image.

To build the image with the written Dockerfile, we need to change directory to the directory containing
the file (in this case, dockerfilev1 folder inside CA4 Part1). 
Then, execute the build command:

```
docker build -t myimagev1 .
```

After building the image, we need to run it to create the container:

```
docker run -p  59001:59001 --name my-container-v1 -d myimagev1:latest
```

After this we can open the container to see if the chat app server is actually running.

We can also, in our host machine, run the client part of the application to connect to the server
and use the chat app windows as seen below:

![](images/part1proofofwork.png)


### Second image

For the second image, the Dockerfile needed to:

* Install Java JDK11
* Copy the jar from the host machine
* Execute the application using the copied jar, exposing the 59001 port

The Dockerfile ended up like:

```
FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y openjdk-11-jdk-headless

WORKDIR /root

COPY basic_demo-0.1.0.jar .

EXPOSE 59001

RUN java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

So of course we needed to provide the jar in the root folder, by building it in the
host machine and copying to the folder where the Dockerfile is:

![](images/part1dockerfilev2jar.png)

Now, just like before we build and run the image to create the container:

```
docker build -t myimagev2 .
```
```
docker run -p  59001:59001 --name my-container-v2 -d myimagev2:latest
```

We can run the client part of the app in the host machine as well, just like in the first
version.

This concludes part 1 of the exercise.

## 1.2. Part 2

The goal of this part of the assignment is to use Docker to setup a containerized environment to
execute our version of the gradle version of the spring basic tutorial application.

We should produce a solution similar to the one of the part 2 of the previous CA
but now using Docker instead of Vagrant.

We also should use docker-compose to produce 2 services/containers:
web: this container is used to run Tomcat and the spring application
db: this container is used to execute the H2 server database

The solution was based on the given repository https://bitbucket.org/atb/docker-compose-spring-tut-demo.

The provided web Dockerfile looked like:

```
FROM tomcat:8-jdk8-temurin

RUN apt-get update -y 

RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://atb@bitbucket.org/atb/tut-basic-gradle-docker.git

WORKDIR /tmp/build/tut-basic-gradle-docker

RUN ./gradlew clean build && cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ && rm -Rf /tmp/build/

EXPOSE 8080
``` 

And changed, so we cloned our own repository and used CA2 Part2 version of the gradle spring basic tutorial application.
We used the one inside CA3 because it already has a connection to the db.
We also gave permissions to access the gradlew file.

So the Dockerfile ended up like this:

``` 
FROM tomcat:9.0.48-jdk11-openjdk-slim

RUN apt-get update -y

RUN apt-get install sudo nano git nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://miguelar97@bitbucket.org/miguelar97/devops-21-22-atb-1151394.git

WORKDIR /tmp/build/devops-21-22-atb-1151394/CA3/CA2/Part2

RUN chmod u+x gradlew
RUN ./gradlew clean build

RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
``` 

Now we can open a command line and change directory to where the docker-compose YAML file is.
We can now run the docker compose build command:

``` 
docker-compose build
``` 

And then execute the command:

``` 
docker-compose up
``` 

As seen in the next images, the docker containers and images were successfully built:

![](images/dockerimagesp2.png)

![](images/dockercontainersp2.png)

We can now see the web application running by using the following url:

http://localhost:8080/basic-0.0.1-SNAPSHOT/

We can also open the H2 console using the following url:

http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console

For the connection string use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb

##Pushing to the hub

As stated before, pushing to DockerHub was not possible.

## Copy db file to volume sync folder

We also need  to copy the db file to volume sync folder - copy the jpadb.mv.db file from the container to our host 
machine.

For that we start by listing the docker containers

> docker ps

This will show all containers:

```
CONTAINER ID   IMAGE                                COMMAND                  CREATED       STATUS         PORTS                                            NAMES
d3b481079708   docker-compose-spring-tut-demo_web   "catalina.sh run"        8 hours ago   Up 4 minutes   0.0.0.0:8080->8080/tcp                           docker-compose-spring-tut-demo-web-1
5dbed5d0823d   docker-compose-spring-tut-demo_db    "/bin/sh -c 'java -c…"   8 hours ago   Up 4 minutes   0.0.0.0:8082->8082/tcp, 0.0.0.0:9092->9092/tcp   docker-compose-spring-tut-demo-db-1
```

We now can reach the db container with the command:

```
docker exec -it 5dbed5d0823d bash
```

If we list the files inside the default path with the ls command, we can see  that a jpadb.mv.db file was created.
We can copy it to the host machine with the command:

```
root@5dbed5d0823d:/usr/src/app# cp /usr/src/app/jpadb.mv.db /usr/src/data-backup
```

This concludes the assignment.

## 3. Alternative Solutions

## 3.1. Part 1

This part of the assignment did not require an alternative.

## 3.2. Part 2

We will be comparing Kubernetes to Docker.

Kubernetes (K8S)

## What is kubernetes ?

Kubernetes, also known as K8s, is an open-source system for automating deployment, scaling, and management 
of containerized applications. Kubernetes is a container orchestration system and can be used with or without Docker.

It is considered to be the market leader and industry-standard orchestration tool for containers
and distributed application deployment.

It can orchestrate different containers on different physical or virtual servers all automatically, based on 
small configurations. In other words, Kubernetes bundles a set of containers into a group that it manages 
on the same machine to reduce network overhead and increase resource usage efficiency.

Kubernetes is particularly useful for DevOps teams since it offers service discovery, load balancing within the cluster,
automated rollouts and rollbacks, self-healing of containers that fail, and configuration management. Plus, Kubernetes
is a critical tool for building robust DevOps CI/CD pipelines.

## What are the benefits of using Kubernetes?

* Automated operations: Kubernetes comes with a powerful API and command line tool, called kubectl, which handles a
bulk of the heavy lifting that goes into container management by allowing you to automate your operations.

* Infrastructure abstraction: Kubernetes manages the resources made available to it on your behalf. This frees 
developers to focus on writing application code and not the underlying compute, networking, or storage infrastructure.

* Service health monitoring: Kubernetes monitors the running environment and compares it against the desired state. 
It performs automated health checks on services and restarts containers that have failed or stopped.

## What is the difference between Docker and Kubernetes?

![](images/kubernetesvsdocker.jpg)

While Docker is a container runtime, Kubernetes is a platform for running and managing containers from many container
runtimes. Kubernetes supports numerous other container runtimes.

So when comparing the two, a better comparison is of Kubernetes with Docker Swarm. Docker Swarm,
is a container orchestration tool like Kubernetes, meaning it allows the management of multiple containers deployed 
across multiple hosts running the Docker server. 

So if both Docker Swarm and Kubernetes are container orchestration platforms, which is the best?

Docker Swarm typically requires less setup and configuration than Kubernetes if you’re building and running your own 
infrastructure.It offers the same benefits as Kubernetes, like deploying your application through declarative YAML
files, automatically scaling services to your desired state, load balancing across containers within the cluster, 
and security and access control across your services.

If you have few workloads running, don’t mind managing your own infrastructure, or don’t need a specific 
feature Kubernetes offers, then Docker Swarm may be a great choice. If you are running many workloads and require cloud
native interoperability, and have many teams in your organization, which creates the need for greater isolation of
services, then Kubernetes is likely the platform you should consider.

## NOTES:
### What is a Container Runtime Interface?

The container runtime is the low-level component that creates and runs containers.
Kubernetes support several container runtime like:

- Docker - [docker.com](https://www.docker.com/)
- CRI-O - [cri-o.io](https://cri-o.io/)
- Containerd - [containerd.io](https://containerd.io/)


