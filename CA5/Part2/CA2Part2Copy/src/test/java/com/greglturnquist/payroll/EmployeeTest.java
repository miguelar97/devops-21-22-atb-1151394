package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    void validateEmployeeFirstName_NullValue(){
        //SUT: Employee constructor and validateEmployeeFirstName() method
        //Arrange
        String firstName = null;
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";

        //Act & Assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeFirstName_EmptyString(){
        //SUT: Employee constructor and validateEmployeeFirstName() method
        //Arrange
        String firstName = "";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeFirstName_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeFirstName() method
        //Arrange
        String firstName = "    ";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeLastName_NullValue(){
        //SUT: Employee constructor and validateEmployeeFirstName() method
        //Arrange
        String firstName = "First";
        String lastName = null;
        String description = "Description";
        String jobTitle = "Job title";


        //Act & Assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeLastName_EmptyString(){
        //SUT: Employee constructor and validateEmployeeLastName() method
        //Arrange
        String firstName = "First";
        String lastName = "";
        String description = "Description";
        String jobTitle = "Job title";


        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeLastName_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeLastName() method
        //Arrange
        String firstName = "First";
        String lastName = "     ";
        String description = "Description";
        String jobTitle = "Job title";

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeDescription_NullValue(){
        //SUT: Employee constructor and validateEmployeeDescription() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = null;
        String jobTitle = "Job title";

        //Act & Assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeDescription_EmptyString(){
        //SUT: Employee constructor and validateEmployeeDescription() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "";
        String jobTitle = "Job title";

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeDescription_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeDescription() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "  ";
        String jobTitle = "Job title";

        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeJobTitle_NullValue(){
        //SUT: Employee constructor and validateEmployeeJobTitle() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = null;


        //Act & Assert
        Assertions.assertThrows(NullPointerException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });

    }

    @Test
    void validateEmployeeJobTitle_EmptyString(){
        //SUT: Employee constructor and validateEmployeeJobTitle() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "";


        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validateEmployeeJobTitle_EmptySpacedString(){
        //SUT: Employee constructor and validateEmployeeJobTitle() method
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "     ";


        //Act & Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
            ;
        });
    }

    @Test
    void validEmployeeState_CompareTwoEmployees(){
        //SUT: Employee constructor and equals/hashCode methods
        //Arrange
        String firstName = "First";
        String lastName = "Last";
        String description = "Description";
        String jobTitle = "Job title";


        //Act
        Employee employeeTest = new Employee(firstName, lastName, description, jobTitle);
        Employee employeeTest2 = new Employee(firstName, lastName, description, jobTitle);

        //Assert
        assertEquals(employeeTest,employeeTest2);
    }
}