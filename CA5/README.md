# Class Assignment 5 Report (CA5)

## Topic: CI/CD Pipelines with Jenkins

## 1. Analysis, Design and Implementation

Jenkins provides an easy-to-use continuous integration system, easier for developers
to integrate project changes, and easier to obtain a fresh build. The automated,
continuous build increases the productivity.

![](images/jenkinsexample.png)

## 1.1. Part 1

For this part of the assignment we should use Jenkins, creating a pipeline, to run the CA2 Part1 gradle application in 
our repository.

## Jenkins Installation

First we need to install Jenkins. There are several ways to do this. I ran the War file directly,
available at https://www.jenkins.io/download/, with the command:

```
java -jar jenkins.war
```

This will run Jenkins and we can Browse to http://localhost:8080 and wait until the Unlock Jenkins page appears.

After choosing the plugins, the password to unlock jenkins should be in Jenkins console log output.

## Creating and configure pipelines

A pipeline is a type of Job.
Build jobs are at the heart of the Jenkins build process. You usually start by creating a new Job.
It can be a complete pipeline or a particular task or step in your build process. In this case it will be a pipeline.

We need to create and configure a pipeline. 
In the Jenkins Dashboard, click on New Item. Give a name to the pipeline (in this case, devops-ca5-part1) and
select the pipeline type, then click ok.

![](images/createpipeline.png)

After this we need to configure the pipeline. The base script (Jenkinsfile) was given in the materials and is available at:

```
https://gist.github.com/atb/d23ce58350c4df272473c7fd7a96838f
``` 

It originally looks like this:

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/luisnogueira/gradle_basic_demo'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                sh './gradlew clean build'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
    }
}
```

Stages are grouping of logical build steps and steps are single tasks.

So we need, as per the exercise, to have four stages in the pipeline:

* Checkout. To checkout the code form the repository.
* Assemble. Compiles and Produces the archive files with the application. Do not use
the build task of gradle (because it also executes the tests).
* Test. Executes the Unit Tests and publish in Jenkins the Test results.
* Archive. Archives in Jenkins the archive files (generated during Assemble).

So we changed our Jenkinsfile to:


```
pipeline {
    agent any
    
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/miguelar97/devops-21-22-atb-1151394'
            }
        }
        stage('Assemble')
        {
            steps {
                echo 'Assembling...'
                dir ("CA2/Part1") {
                    script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew assemble'}
                    else
                        bat './gradlew assemble'
                }
                }
            }
        }
        stage('Test') {
            steps {
                echo 'testing...'
                dir ("CA2/Part1"){
                     script {
                    if (isUnix()){
                        sh 'chmod +x gradlew'
                        sh './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                    else
                    {
                        bat './gradlew test'
                        junit 'build/test-results/test/*.xml'}
                }
                }
            }
        }       
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir ("CA2/Part1"){
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }
}
```

In the Checkout stage we simply clone the repository.

In the Assemble stage, we run the gradle assemble task, after changing the directory to CA2/Part1. Note that there is an if statement inside, that will
check if the system that jenkins is running on is Unix based or not. This will change the way the command is run
for each operating system. This ensures this pipeline is correctly configured to run in any kind of system and not
limited to the one used when configuring.

In the Test stage, we run the gradle test task, while also using the junit line to read the xml file with the test
results and show on the jenkins page. This stage also checks for which operating system it is running on.

Lastly the Archiving stage changes directory to CA2/Part1 and uses archiveArtifacts to copy the files inside
build/distributions.

Now we can run the Jenkinsfile. As seen in the image below it ran successfully after some changes.

![](images/jenkinspipelineallstepsgreen.png)

This concludes this part of the assignment.

## 1.2. Part 2

For this assignment, the is to create a pipeline in Jenkins to build the
tutorial spring boot application, gradle "basic" version (developed in CA2, Part2).

The pipeline should have the following stages:

* Checkout: To checkout the code from the repository.
* Assemble: Compiles and Produces the archive files with the application. Note: do not use the build task of gradle 
because it will also execute the tests.
* Test: Executes the Unit Tests and publish in Jenkins the Test results. 
* Javadoc: Generates the javadoc of the project and publish it in Jenkins.
* Archive: Archives in Jenkins the archive files (generated during Assemble, i.e., the war
  file)
* Publish image: Generate a docker image with Tomcat and the war file and publish it
  in the Docker Hub.

To be able to develop our solution, we needed to install a few plugins via the Plugin Manager:

```
Javadoc Plugin - This plugin adds Javadoc support to Jenkins.

HTML Publisher plugin - This plugin publishes HTML reports.

JUnit - Allows JUnit-format test results to be published.
```

With these requirements we developed the following Jenkinsfile:

```
pipeline {
    agent any
   
    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/miguelar97/devops-21-22-atb-1151394'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir ("CA5/Part2/CA2Part2Copy") {
                    script {
                        if (isUnix())
                            sh '''
                            chmod +x gradlew
                            ./gradlew clean assemble
                            '''
                        else
                            bat './gradlew assemble'
                    }
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                 dir ("CA5/Part2/CA2Part2Copy") {
                    script {
                        if (isUnix()){
                            sh './gradlew test'
                            junit 'build/test-results/test/*.xml'
                        }else{
                            bat './gradlew test'
                            junit 'build/test-results/test/*.xml'
                        }
                    }
                }
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Generating javadoc...'
                 dir ("CA5/Part2/CA2Part2Copy") {
                    script {
                        javadoc javadocDir: 'build/reports/tests/test', keepAll: false
                    }
                }
            }
        }
        stage('HTML') {
            steps {
                echo 'documenting...'
                 dir ("CA5/Part2/CA2Part2Copy"){
                    publishHTML([
                        allowMissing: false, 
                        alwaysLinkToLastBuild: false, 
                        keepAll: false, 
                        reportDir: 'build/reports/tests/test', 
                        reportFiles: 'index.html', 
                        reportName: 'HTML Report', 
                        reportTitles: 'Docs Loadtest Dashboard'
                        ])
                }
            }
        }
        stage('Archive') {
            steps {
                echo 'Archiving...'
                 dir ("CA5/Part2/CA2Part2Copy"){
                    archiveArtifacts 'build/libs/'
                }
            }
        }
        stage('Docker Image') {
            steps {
                echo 'Creating docker image...'
                script {
                     dir ("CA5/Part2/CA2Part2Copy"){
                        dockerImage = docker.build("ca5part2:${env.BUILD_ID}")
                    }
                }
            }
        }
    
    }
}
```

As we can see the last stage was omitted, which was responsible for pushing the created docker image to the 
Docker Hub. However as seen in the past exercises there was a problem with Docker Hub that could not be adressed
and it was decided with the Teacher (ATB) that it was ok to skip this step.

Still, the stage would be something like:

```
stage('Docker Deploy') { 
            steps { 
                echo 'Deploying docker image...'
                script { 
                    docker push miguelaraujo97/ca5part2
                } 
            }
        }
```

The Jenkinsfile was run successfully:

![](images/jenkinsca5part2finish.png)

And the docker image was created:

![](images/dockerca5part2.png)

This concludes the second part of the assignment.

## 3. Alternative Solutions

## 3.1. Part 1

This part of the assignment did not require an alternative.

## 3.2. Part 2



